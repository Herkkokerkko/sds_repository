package com.example.firstmobileapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
             public void onClick(View v) {
                 EditText editTextTextPersonName = (EditText) findViewById(R.id.editTextTextPersonName);
                EditText editTextTextPersonName2 = (EditText) findViewById(R.id.editTextTextPersonName2);
                TextView textView = (TextView) findViewById(R.id.textView);

                int num1 = Integer.parseInt(editTextTextPersonName.getText().toString());
                int num2 = Integer.parseInt(editTextTextPersonName2.getText().toString());
                int result = num1 + num2;
                textView.setText(result + "");

    }
        });
    }
}